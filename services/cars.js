import * as CONFIG from '../common/config.js';

function options_get(){
    return{
        headers:{
            'Accept': 'application/json',
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + localStorage.getItem('token')
        },
        method:'get',
    };
}

function options_post(body){
    return {
        headers:{
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + localStorage.getItem('token')
        },
        method:'post',
        body:JSON.stringify(body)
    }
};

export function createCheckout(body){
    let urlCourse = CONFIG.HTTP_URL+"/auth/checkoutCar";
    return new Promise(function(resolve, reject){
        fetch(urlCourse,options_post(body))
            .then(response => response.json())
            .then(json => {
                resolve(json);
            } );
    });
}

export function createCheckback(body){
    let urlCourse = CONFIG.HTTP_URL+"/auth/checkbackCar";
    return new Promise(function(resolve, reject){
        fetch(urlCourse,options_post(body))
            .then(response => response.json())
            .then(json => {
                resolve(json);
            } );
    });
}

// get method
export function getCarList(page){
    let url = CONFIG.HTTP_URL+"/cars?page=" + page;

    return new Promise(function(resolve, reject){
        fetch(url,options_get())
            .then(response => response.json())
            .then(json => {
                resolve(json);
            } ).catch(function(e) {
                console.error(e); // "Uh-oh!"
             })

        ;
    });
}