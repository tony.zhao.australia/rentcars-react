import * as CONFIG from '../common/config.js';

const options_get = {
    headers:{
        'Accept': 'application/json',
        'Content-Type': 'application/json',
    },
    method:'get',
};

function options_post(body){
    return {
        headers:{
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        },
        method:'post',
        body:JSON.stringify(body)
    }
};

export function login(body){
    let urlCourse = CONFIG.HTTP_URL+"/auth/login";
    return new Promise(function(resolve, reject){
        fetch(urlCourse,options_post(body))
            .then(response => response.json())
            .then(json => {
                resolve(json);
            } );
    });
}