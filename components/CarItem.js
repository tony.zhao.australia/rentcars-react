import React, { Component } from 'react';
import Router from 'next/router'
import styles from "../styles/Home.module.css";

import Box from '@mui/material/Box';
import Button from '@mui/material/Button';
import Typography from '@mui/material/Typography';
import Modal from '@mui/material/Modal';
import TextField from '@mui/material/TextField';
import Radio from '@mui/material/Radio';
import RadioGroup from '@mui/material/RadioGroup';
import FormControlLabel from '@mui/material/FormControlLabel';
import FormControl from '@mui/material/FormControl';
import FormLabel from '@mui/material/FormLabel';


import { login, } from '../services/auth';
import { createCheckout, createCheckback } from '../services/cars';

const style = {
    position: 'absolute',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    width: 600,
    bgcolor: 'background.paper',
    border: '2px solid #000',
    boxShadow: 24,
    p: 4,
};

export default class CarItem extends Component {

    constructor(props) {
        super(props);

        this.state = {
            open: false,
            username:'',
            password:'',
            openCheckout: false,
            openCheckback: false,
            startTimeString:'2021-11-01',
            backTimeString:'2021-11-01',
            errMsgCheckout: '',
            errMsgCheckback: '',
            errMsgLogin: '',
            backCondition: 1,
            customerName: localStorage.getItem('name')

        }
    }

    handleClickCheckout(){

        let token = localStorage.getItem('token')

        if(token){

            this.setState({
                openCheckout: true
            })

        }else{
            this.setState({
                open: true
            })
        }

    }

    handleCloseBtn(){
        this.setState({
            open: false
        })
    }

    handleCloseCheckoutModalBtn(){
        this.setState({
            openCheckout: false
        })
    }

    async handleClickSubmit(e){
        e.preventDefault()

        let username = this.state.username
        let password = this.state.password
        console.log(username)
        console.log(password)

        let credential = {
            "email": username,
            "password": password
        }

        let tokenRes = await login(credential)
        console.log('tokenRes', tokenRes)

        if(tokenRes.success == 'no'){
            this.setState({
                errMessage: tokenRes.error,
            })
        }else{
            localStorage.setItem('token', tokenRes.access_token);
            localStorage.setItem('name', tokenRes.name);
            localStorage.setItem('email', tokenRes.email);

            this.setState({
                open: false
            })

            location.reload()
        }
    }

    handleChangeUsername(e){
        this.setState({
            username: e.target.value
        })

    }

    handleChangePassword(e){
        this.setState({
            password: e.target.value
        })

    }

    handleChangeYourname(e){
        this.setState({
            customerName: e.target.value
        })

    }

    handleStartTimeChange(e){

        this.setState({
            startTimeString: e.target.value
        })

    }

   async handleSubmitCheckoutCar(){
        let body = {
            makeId: this.props.make_id,
            checkoutType: 1,
            customerName : this.state.customerName,
            checkoutDate : this.state.startTimeString,
        }
        let res = await createCheckout(body)

        if(res.success == 'yes'){
            this.setState({
                openCheckout: false
            })

            location.reload();
        }else{
            this.setState({
                errMsgCheckout: res.msg.errorMsg
            })
        }

    }

    async handleSubmitCheckbackCar(e){
        let body = {
            makeId: this.props.make_id,
            checkbackAt: this.state.backTimeString,
            checkbackCondition: this.state.backCondition

        }
        let res = await createCheckback(body)

        if(res.success == 'yes'){
            this.setState({
                openCheckback: false
            })

            location.reload();
        }else{
            this.setState({
                errMsgCheckback: res.msg.errorMsg
            })
        }
    }

    handleClickCheckback(){
        this.setState({
            openCheckback: true
        })
    }

    handleBackTimeChange(e){
        this.setState({
            backTimeString: e.target.value
        })
    }

    handleConditionChange(e){
        this.setState({
            backCondition: e.target.value
        })
    }



    render() {

        let disabled = (this.props.status==0 && this.props.if_owner == false)?true:false

        console.log(this.props.if_owner)
        return (
            <a  className={styles.card}>
                <h4>{"ID : " + this.props.make_id}</h4>
                <h2>{this.props.make_name} </h2>
                <p>{this.props.type_name}</p>
                <Button variant="contained"
                        onClick={()=>{this.handleClickCheckout()}}
                        disabled={disabled}
                        style={{display: this.props.if_owner == true ? 'none':'block'}}

                >Checkout</Button>

                <Button variant="contained"
                        color="success"
                        onClick={()=>{this.handleClickCheckback()}}
                        style={{display: this.props.if_owner == true ? 'block':'none'}}

                >Checkback</Button>

                <Modal
                    open={this.state.open}

                    aria-labelledby="modal-modal-title"
                    aria-describedby="modal-modal-description"
                >
                    <Box sx={style}>
                        <Typography id="modal-modal-title" variant="h6" component="h2">
                            Please login
                        </Typography>
                        <Typography id="modal-modal-description" sx={{ mt: 2 }}>
                            <TextField
                                style={{width:'400px'}}
                                value={this.state.username}
                                required id="outlined-basic" label="Email" variant="outlined"
                                onChange={(e)=>{this.handleChangeUsername(e)}}
                            />
                            <br/>
                            <br/>
                            <br/>
                            <TextField
                                style={{width:'400px'}}
                                value={this.state.password}
                                id="outlined-password-input"
                                label="Password"
                                type="password"
                                autoComplete="current-password"
                                onChange={(e)=>{this.handleChangePassword(e)}}
                            />

                        </Typography>

                        <br/>
                        <br/>

                        <Button variant="contained" onClick={(e)=>{this.handleClickSubmit(e)}}>Submit</Button>
                        <Button variant="outlined" onClick={()=>{this.handleCloseBtn()}} style={{marginLeft:'30px'}}>Cancel</Button>

                    </Box>
                </Modal>

                <Modal
                    open={this.state.openCheckout}

                    aria-labelledby="modal-modal-title"
                    aria-describedby="modal-modal-description"
                >
                    <Box sx={style}>
                        <Typography id="modal-modal-title" variant="h6" component="h2">
                            Checkout your car
                            <br/>
                            ID: {this.props.make_id}
                            <br/>
                            Make Name: {this.props.make_name}
                        </Typography>
                        <hr/>
                        <Typography id="modal-modal-description" sx={{ mt: 2 }}>
                            <br/>
                            <TextField
                                style={{width:'400px'}}
                                value={this.state.customerName}
                                required id="outlined-basic" label="Your Name" variant="outlined"
                                onChange={(e)=>{this.handleChangeYourname(e)}}
                            />
                            <br/>
                            <br/>
                            <br/>

                            Checkout at: <br/>
                            <input type="date" id="start" name="trip-start"
                                   value={this.state.startTimeString}
                                   onChange={(e)=>{this.handleStartTimeChange(e)}}
                                   min="2018-01-01" max="2028-12-31" />


                        </Typography>

                        <br/>
                        <br/>

                        <Button variant="contained" onClick={(e)=>{this.handleSubmitCheckoutCar(e)}}>Submit</Button>
                        <Button variant="outlined" onClick={()=>{this.handleCloseCheckoutModalBtn()}} style={{marginLeft:'30px'}}>Cancel</Button>
                        <p>{this.state.errMsgCheckout}</p>
                    </Box>
                </Modal>


                <Modal
                    open={this.state.openCheckback}

                    aria-labelledby="modal-modal-title"
                    aria-describedby="modal-modal-description"
                >
                    <Box sx={style}>
                        <Typography id="modal-modal-title" variant="h6" component="h2">
                            Checkback your car
                            <br/>
                            ID: {this.props.make_id}
                            <br/>
                            Make Name: {this.props.make_name}
                        </Typography>

                        <hr/>

                        <Typography id="modal-modal-description" sx={{ mt: 2 }}>

                            <FormControl component="fieldset">
                                <FormLabel component="legend">Condition</FormLabel>
                                <RadioGroup row aria-label="gender" name="row-radio-buttons-group"
                                            value={this.state.backCondition}
                                            onChange={(e)=>{this.handleConditionChange(e)}}
                                >
                                    <FormControlLabel value={1} control={<Radio />} label="Good" />
                                    <FormControlLabel value={2} control={<Radio />} label="Normal" />
                                    <FormControlLabel value={3} control={<Radio />} label="Bad" />
                                </RadioGroup>
                            </FormControl>
                            <br/>
                            <br/>

                            Checkback at: <br/>
                            <input type="date" id="start" name="trip-start"
                                   value={this.state.backTimeString}
                                   onChange={(e)=>{this.handleBackTimeChange(e)}}
                                   min="2018-01-01" max="2028-12-31" />


                        </Typography>

                        <br/>
                        <br/>

                        <Button variant="contained" onClick={(e)=>{this.handleSubmitCheckbackCar(e)}}>Submit</Button>
                        <Button variant="outlined" onClick={()=>{this.setState({openCheckback: false})}} style={{marginLeft:'30px'}}>Cancel</Button>
                        <p>{this.state.errMsgCheckback}</p>
                    </Box>
                </Modal>
            </a>

        );
    }
}